# Copyright 2020 Michael Green
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

using Distributions
using LinearAlgebra


"""
    estimatemlμ(Y, X)

Maximum Likelihood (ML) estimate of the parameters B for a multivariate linear regression with
m response variables, (k-1) predictors and n observations.

# Arguments:
- `Y`: Response matrix of n rows(observations) and m responses(outputs)
- `X`: Covariate matrix of n rows(observations) and k variables of which one should exclusively filled with 1
"""
function estimatemlμ(Y, X)
    inv(X'X .+ eps() * 1000) * X' * Y
end


"""
    estimatemapμ(Y, X, Λ₀, B₀)

Maximum A Postiori (MAP) estimate of the parameters B for a multivariate linear regression with
m response variables, (k-1) predictors and n observations.

# Arguments:
- `Y`: Response matrix of n rows(observations) and m responses(outputs)
- `X`: Covariate matrix of n rows(observations) and k variables of which one should exclusively filled with 1
- `Λ₀`: Prior scale matrix (among-row variance)
- `B₀`: Prior mean matrix of the parameters of the multivariate regression
"""
function estimatemapμ(Y, X, Λ₀, B₀)
    Λₙ = X'X + Λ₀
    Bₙ = inv(Λₙ) * (X'Y + Λ₀ * B₀)
    return Bₙ
end


"""
    mvpostσ(Y, X, Λ₀, V₀, ν₀, B₀)

Conditional Posterior of the covariance matrix of the parameters B for a
multivariate linear regression with m response variables, (k-1) predictors
and n observations

# Arguments:
- `Y`: Response matrix of n rows(observations) and m responses(outputs)
- `X`: Covariate matrix of n rows(observations) and k variables of which one should exclusively filled with 1
- `Λ₀`: Prior scale matrix (among-row precision)
- `V₀`: Prior scale matrix (among-column variance)
- `ν₀`: Prior degrees of freedom which will go into an inverse Wishart distribution
- `B₀`: Prior mean matrix of the parameters of the multivariate regression
"""
function mvpostσ(Y, X, Λ₀, V₀, ν₀, B₀)
    # Λ₀ is a kxk matrix
    Λₙ = X'X + Λ₀
    Bₙ = inv(Λₙ) * (X'Y + Λ₀ * B₀)
    # V₀ is a mxm matrix
    Vₙ = V₀ + (Y - X * Bₙ)' * (Y - X * Bₙ) + (Bₙ - B₀)' * Λ₀ * (Bₙ - B₀)
    νₙ = ν₀ + size(X)[1]
    Σ = InverseWishart(νₙ, Vₙ)
end

"""
    mvpostμ(Y, X, Λ₀, V₀, ν₀, B₀, ξ)

Conditional Posterior of the parameters B for a multivariate linear regression with
m response variables, (k-1) predictors and n observations

# Arguments:
- `Y`: Response matrix of n rows(observations) and m responses(outputs)
- `X`: Covariate matrix of n rows(observations) and k variables of which one should exclusively filled with 1
- `Λ₀`: Prior scale matrix (among-row precision) size k*k
- `V₀`: Prior scale matrix (among-column variance) size m*m
- `ν₀`: Prior degrees of freedom which will go into an inverse Wishart distribution
- `B₀`: Prior mean matrix of the parameters of the multivariate regression
- `ξ`: Turns the IW prior into a Scaled IW prior which is optional. The number of elements should be m. If you want the normal IW prior just set ξ to [1,1,...,1]
"""
function mvpostμ(Y, X, Λ₀, V₀, ν₀, B₀, ξ)
    # Λ₀ is a kxk matrix
    Λₙ = X'X + Λ₀
    Bₙ = inv(Λₙ) * (X'Y + Λ₀ * B₀)
    # V₀ is a mxm matrix
    Vₙ = V₀ + (Y - X * Bₙ)' * (Y - X * Bₙ) + (Bₙ - B₀)' * Λ₀ * (Bₙ - B₀)
    νₙ = ν₀ + size(X)[1]
    Σ = rand(InverseWishart(νₙ, Vₙ))
    Σ = diagm(ξ) * Σ * diagm(ξ)
    MatrixNormal(Bₙ, inv(Hermitian(Λₙ)), Σ)
end

"""
    predict(X, θ)

Prediction of responses based on matrix X and parameters θ
"""
predict(X, θ) = X * θ


"""
    mvmarμ(Y, X, Λ₀, V₀, ν₀, B₀, ξ)

This implements the marginal posterior distribution p(μ|Y,X), i.e., where
we have integrated out the Σ from the conditional distribution p(μ|Σ,Y,X).
WARNING: do not use this for now. It's broken.

# Arguments:
- `Y`: Response matrix of n rows(observations) and m responses(outputs)
- `X`: Covariate matrix of n rows(observations) and k variables of which one should exclusively filled with 1
- `Λ₀`: Prior scale matrix (among-row precision) size k*k
- `V₀`: Prior scale matrix (among-column variance) size m*m
- `ν₀`: Prior degrees of freedom which will go into an inverse Wishart distribution
- `B₀`: Prior mean matrix of the parameters of the multivariate regression
- `ξ`: Turns the IW prior into a Scaled IW prior which is optional. The number of elements should be m. If you want the normal IW prior just set ξ to [1,1,...,1]
"""
function mvmarμ(Y, X, Λ₀, V₀, ν₀, B₀, ξ)
    # Λ₀ is a kxk matrix
    Λₙ = X'X + Λ₀
    Bml = estimatemlμ(Y, X)
    Blc = X' * X * Bml + Λ₀ * B₀ # This term is a linear combination of the ML estimation and the prior mean B₀
    Bₙ = inv(Hermitian(Λₙ)) * Blc # Weight the linear combination by the respective precision matrix
    #U = round.(inv(Λₙ), digits=12) # This is an ugly hack which should not be needed
    U = inv(Hermitian(Λₙ))
    #Vₙ =  Y'Y + B₀'*Λ₀*B₀ - Blc'*inv(Λₙ)*Blc # Got this from http://bictel.ulg.ac.be/ETD-db/collection/available/ULgetd-12192012-155142/unrestricted/thesis.pdf but it doesn't work
    Vₙ = V₀ + (Y - X * Bₙ)' * (Y - X * Bₙ) + (Bₙ - B₀)' * Λ₀ * (Bₙ - B₀)
    νₙ = ν₀ + size(X)[1]
    MatrixTDist(ν₀, Bₙ, U, Vₙ)
end


"""
    generatepredictfn(Y, X, Λ₀, V₀, ν₀, B₀, ξ)

Creates a prediction function for a new dataset X. This function samples from
the Conditional Posterior of the parameters B for a multivariate linear regression with
m response variables, (k-1) predictors and n observations

# Arguments:
- `Y`: Response matrix of n rows(observations) and m responses(outputs)
- `X`: Covariate matrix of n rows(observations) and k variables of which one should exclusively filled with 1
- `Λ₀`: Prior precision matrix (among-row precision) k×k matrix
- `V₀`: Prior scale matrix for responses (among-column variance) m×m matrix
- `ν₀`: Prior degrees of freedom which will go into an inverse Wishart distribution
- `B₀`: Prior mean matrix of the parameters of the multivariate regression
- `ξ`: Turns the IW prior into a Scaled IW prior which is optional. The number of elements should be m. If you want the normal IW prior just set ξ to [1,1,...,1]
"""
function generatepredictfn(Y, X, Λ₀, V₀, ν₀, B₀, ξ)
    # Λ₀ is a kxk matrix
    Λₙ = X'X + Λ₀
    Bₙ = inv(Λₙ) * (X'Y + Λ₀ * B₀)
    # V₀ is a mxm matrix
    Vₙ = V₀ + (Y - X * Bₙ)' * (Y - X * Bₙ) + (Bₙ - B₀)' * Λ₀ * (Bₙ - B₀)
    νₙ = ν₀ + size(X)[1]
    Σ = InverseWishart(νₙ, Vₙ)
    U = inv(Hermitian(Λₙ))
    # Return function to use for predictions
    function f(X)
        V = diagm(ξ) * rand(Σ) * diagm(ξ)
        μ = MatrixNormal(Bₙ, U, V)
        θ = rand(μ)
        X * θ
    end
    f
end

function mvposteriorpred(Y, X, Λ₀, V₀, ν₀, B₀, ξ)
    Λₙ = X'X + Λ₀
    Bₙ = inv(Λₙ) * (X'Y + Λ₀ * B₀)
end
