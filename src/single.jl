using Distributions
using LinearAlgebra

"""
    posterior(X, y, m, s, β = 1 / sqrt(var(y)))

Return the posterior distribution for the parameters of a Bayesian Multiple
Linear Regression. Thus the distribution that is returned is ``p(θ|y,X)``.

# Arguments:
- `X`: The matrix containing the covariates
- `y`: The vector containing the response to model
- `m`: The mean vector of the prior Normal distribution for each parameter
- `s`: The precision vector of the prior Normal distribution for each parameter
- `β`: The precision of the response variable
"""
function posterior(X, y, m, s, β = 1 / sqrt(var(y)))
    k = size(X)[2]
    m₀ = m
    S₀ = Matrix{Float32}(I, k, k) ./ s
    invSₙ = inv(S₀) + β * X'X
    mₙ = inv(invSₙ) * (inv(S₀) * m₀ + β * X'y)
    mₙ = reshape(mₙ, k)
    ωd = MvNormal(mₙ, round.(inv(invSₙ), digits = 12))
    ωd
end

"""
    posterior(X, y)

Return the posterior distribution for the parameters of a Bayesian Multiple
Linear Regression where the priors are set with a rather uninformative prior.
Thus the distribution that is returned is ``p(θ|y,X)``.

# Arguments:
- `X`: The matrix containing the covariates
- `y`: The vector containing the response to model
"""
function posterior(X, y)
    k = size(X)[2]
    m₀ = zeros(k)
    s₀ = ones(k) * 1/500
    β = 1 / sqrt(var(y))
    posterior(X, y, m₀, s₀, β)
end


"""
    posteriorpred(X, y, m, s, β = 1 / sqrt(var(y)))

Return the posterior predictive distribution for the output of a Bayesian
Multiple Linear Regression. This reuturns a function which takes a single input
`x` representing one datapoint and outputs a distribution which can be sampled
using `rand`. It's important to note that sampling from this predictive
distribution is not the same as sampling the posterior parameters and multiply
with the covariates since the former inlcudes the full uncertainty. This means
the uncertainty coming from the estimates of the parameters and the uncertinaty
coming from the data generating process. Thus the distribution that is returned
is ``p(ŷ|θ, y, X)`` which naturally contains more varaiance since it has noise
from the response variable as well as the noise from the estimation of the
parameters.

# Arguments:
- `X`: The matrix containing the covariates
- `y`: The vector containing the response to model
- `m`: The mean vector of the prior Normal distribution for each parameter
- `s`: The precision vector of the prior Normal distribution for each parameter
- `β`: The precision of the response variable
"""
function posteriorpred(X, y, m, s, β = 1 / sqrt(var(y)))
    ωd = posterior(X, y, m, s, β)
    mₙ = mean(ωd)
    Sₙ = cov(ωd)
    function f(x)
        Normal(mₙ'x, 1/β + x'Sₙ*x)
    end
end

