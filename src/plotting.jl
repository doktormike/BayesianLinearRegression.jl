# Copyright 2020 Michael Green
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

using UnicodePlots

"""
    plotposterior(Bpost, index=1, nsamples=1000)

The posterior of the matrixvariate normal distribution is plotted
using the boxplot from UnicodePlots.

# Arguments:
- `Bpost`: The posterior represented as a Matrixvariate Normal Distribution
- `index`: The index of the response variable to plot the parameters for
- `nsamples`: The number of samples to pull from the posterior
"""
function plotposterior(Bpost, index = 1, nsamples = 1000)
    θ = rand(Bpost)
    nvars = size(θ)[1]
    b = [[rand(Bpost)[j, index] for i = 1:nsamples] for j = 1:nvars]
    names = ["b" * string(i) for i = 1:nvars]
    boxplot(names, b, width = 80)
end

