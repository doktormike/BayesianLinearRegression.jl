module BayesianLinearRegression

export estimatemapμ, estimatemlμ, mvpostμ, mvpostσ, plotposterior
export mvmarμ, generatepredictfn

export posterior, posteriorpred

include("posterior.jl")
include("single.jl")
include("plotting.jl")


end # module
