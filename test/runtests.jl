using BayesianLinearRegression
using LinearAlgebra
using Test
using CSV
using DataFrames
using StatsBase
using Distributions
# using AutomaticDocstrings

function test4by3()
    # Create dummy data
    X = hcat(sin.(1:10), cos.(1:10), tanh.(-5:4), [1 for i in 1:10])
    B = [1.0 2.0 3.0
        1.2 2.2 3.2
        1.4 2.4 3.4
        5.0 6.0 7.0]
    Y = X * B + rand(Normal(0, 0.1), 10, 3)
    #lineplot(Y[:,1],  width=80)
    #lineplot(Y[:,2],  width=80)

    B₀ = zeros(4, 3)
    Λ₀ = Matrix{Float64}(I, 4, 4)
    V₀ = Matrix{Float64}(I, 3, 3)
    ν₀ = 100

    Λₙ = X'X + Λ₀
    Bₙ = inv(Λₙ) * (X'Y + Λ₀ * B₀)

    # V₀ is a mxm matrix
    Vₙ = V₀ + (Y - X * Bₙ)' * (Y - X * Bₙ) + (Bₙ - B₀)' * Λ₀ * (Bₙ - B₀)
    νₙ = ν₀ + size(X)[1]

    @show Bₙ, νₙ, Vₙ

    Σ = rand(InverseWishart(νₙ, Vₙ))

    rand(MatrixNormal(Bₙ, Λₙ, Σ))

    bpost = posterior(Y, X, Λ₀, V₀, ν₀, B₀)
    rand(bpost)
end

function genfakedata()
    # Prepare data which consists of 5 input x1-x5 and 6 outputs y1-y6
    # In the end we add a bias x6
    # myloc = joinpath(dirname(pathof(MyPkg)), "..", "data")
    myloc = joinpath(dirname(pathof(BayesianLinearRegression)), "../", "data/fake_series_alviss.csv")
    #myloc = "../data/fake_series_alviss.csv"
    df = CSV.read(myloc, DataFrame)
    X = df[:, 2:6]
    Y = df[:, 7:12]
    X[:, :x6] .= 1.0
    X = Matrix(X)
    Y = Matrix(Y)
    return Y, X
end

function testmlmapsimilarity()
    Y, X = genfakedata()
    # Set priors
    k, m = size(X)[2], size(Y)[2]
    B₀ = zeros(k, m)
    Λ₀ = Matrix{Float64}(I, k, k)
    # MAP and ML estimate
    μmap = estimatemapμ(Y, X, Λ₀, B₀)
    μml = estimatemlμ(Y, X)
    # Predict
    Ymap = X * μmap
    Yml = X * μml
    return Ymap - Yml
end

function genmlmapsolution(Y, X)
    k, m = size(X)[2], size(Y)[2]
    B₀ = zeros(k, m)
    Λ₀ = Matrix{Float64}(I, k, k)
    # MAP and ML estimate
    μmap = estimatemapμ(Y, X, Λ₀, B₀)
    μml = estimatemlμ(Y, X)
    # Predict
    Ymap = X * μmap
    Yml = X * μml
    return Ymap, Yml
end

function genposterior(Y, X)
    k, m = size(X)[2], size(Y)[2]
    B₀ = zeros(k, m)
    Λ₀ = Matrix{Float64}(I, k, k)
    V₀ = Matrix{Float64}(I, m, m)
    ν₀ = 1
    ξ = ones(size(Y)[2])
    bpost = BayesianLinearRegression.mvpostμ(Y, X, Λ₀, V₀, ν₀, B₀, ξ)
    return bpost
end

function genmarginalposterior(Y, X)
    k, m = size(X)[2], size(Y)[2]
    B₀ = zeros(k, m)
    Λ₀ = Matrix{Float64}(I, k, k)
    V₀ = Matrix{Float64}(I, m, m)
    ν₀ = 1
    ξ = ones(size(Y)[2])
    bpost = BayesianLinearRegression.mvmarμ(Y, X, Λ₀, V₀, ν₀, B₀, ξ)
    return bpost
end

function genfpred(Y, X)
    k, m = size(X)[2], size(Y)[2]
    B₀ = zeros(k, m)
    Λ₀ = Matrix{Float64}(I, k, k)
    V₀ = Matrix{Float64}(I, m, m)
    ν₀ = 1
    ξ = ones(size(Y)[2])
    f = BayesianLinearRegression.generatepredictfn(Y, X, Λ₀, V₀, ν₀, B₀, ξ)
    return f
end


function testfakedataposterior()
    # Prepare data which consists of 5 input x1-x5 and 6 outputs y1-y6
    # In the end we add a bias x6

    df = CSV.File("/home/michael/datasets/timeseries/fake_series_alviss.csv") |> DataFrame!
    X = df[:, 2:6]
    Y = df[:, 7:12]
    X[:, :x6] .= 1.0
    X = round.(Matrix(X); digits=3)
    Y = round.(Matrix(Y); digits=3)

    # Set priors
    k = size(X)[2]
    m = size(Y)[2]
    B₀ = zeros(k, m)
    Λ₀ = Matrix{Float64}(I, k, k)
    V₀ = Matrix{Float64}(I, m, m)
    ν₀ = 1

    # SILLY
    Λₙ = X'X + Λ₀
    Bₙ = inv(Λₙ) * (X'Y + Λ₀ * B₀)
    ξ = ones(size(Y)[2])
    Σpost = BayesianLinearRegression.mvpostσ(Y, X, Λ₀, V₀, ν₀, B₀)
    Σ = rand(Σpost)
    if !(isposdef(Σ) && isposdef(Bₙ) && isposdef(inv(Λₙ)))
        @show eigen(inv(Λₙ)).values
        @show eigen(Σ).values
    end

    Uone = inv(Λₙ)
    Utwo = conj(Λₙ) * inv(Λₙ * conj(Λₙ))
    # SILLY

    # Posterior
    bpost = BayesianLinearRegression.mvpostμ(Y, X, Λ₀, V₀, ν₀, B₀, ξ)

    # Predict
    Ŷ = X * rand(bpost)
    Ŷ - Y

    plt = scatterplot(1:100, Yml[:, 3], color=:red)
    scatterplot!(plt, 1:100, Ypost[:, 3], color=:blue)

    plt = scatterplot(1:100, Y[:, 1], color=:red, width=80, height=30)
    Ŷ = X * rand(bpost)
    scatterplot!(plt, 1:100, Ŷ[:, 1], color=:blue)
    Ŷ = X * rand(bpost)
    scatterplot!(plt, 1:100, Ŷ[:, 1], color=:green)
    Ŷ = X * rand(bpost)
    scatterplot!(plt, 1:100, Ŷ[:, 1], color=:yellow)

end

function genfakemmmdata()
    N = 52 * 3
    # Season and Macro Variable
    xs = sin.((1:N) ./ (2π))
    xe = (sin.((1:N) ./ (5 * 2π)) .+ 1.1) ./ 10
    # Media
    xm1 = rand(Binomial(20, 0.02), N) * 10000
    xm2 = rand(Binomial(20, 0.02), N) * 10000
    xm3 = rand(Binomial(20, 0.02), N) * 10000
    # Targets
    y1 = 1000 .+ 500 * xs .+ 10_000 * xe .+ 0.1 * xm1 .+ 0.1 * xm2 .+ 0.1 * xm3 .+ 100 * randn(N)
    y2 = 4000 .+ 1000 * xs .+ -10_000 * xe .+ 0.2 * xm1 .+ 0.2 * xm2 .+ 0.2 * xm3 .+ 100 * randn(N)
    # p = lineplot(y1, width=120, color=:green)
    # lineplot!(p, y2, color=:red)
    # Matricies
    y = [y1 y2]
    X = [ones(N) xs xe xm1 xm2 xm3]
    (X=X, y=y)
end

@testset "Posterior and OLS for β approximately the same" begin
    # Data
    X, Y = genfakemmmdata()

    # ML predictions
    Ymap, Yml = genmlmapsolution(Y, X)
    # ML β
    βml = estimatemlμ(Y, X)

    # Posterior β
    k, m = size(X)[2], size(Y)[2]
    # Assome prior β are 0
    B₀ = zeros(k, m)
    # Scale Priors for betas
    # NOTE: 1:3 is the bias, season and macro which all are in the scale of 1_000 and 10_000
    Λ₀ = Matrix{Float64}(I, k, k)
    Λ₀[:, 1:2] *= 1 / 1000 # Bias and season prior
    Λ₀[:, 3] *= 1 / 10_000 # Economy prior
    # Scale priors for between target variation
    V₀ = Matrix{Float64}(I, m, m)
    ν₀ = 1
    # Additional scaling variable (most likely not very useful) = 1
    ξ = ones(size(Y)[2])
    # Posterior as a MatrixDistibution (Each random sample pulls a matrix of size k x m)
    β = BayesianLinearRegression.mvpostμ(Y, X, Λ₀, V₀, ν₀, B₀, ξ)

    # Compare relative difference of mean to OLS (Should be very similar due to our wide priors)
    @test maximum(1 .- mean(β) ./ βml) < 0.01
end

@testset "Posterior and ML approaximately same" begin
    # Write your own tests here.
    Y, X = genfakedata()
    Ymap, Yml = genmlmapsolution(Y, X)
    Bpost = genposterior(Y, X)
    fpred = genfpred(Y, X)
    Ypost = X * rand(Bpost)
    Ypredfun = fpred(X)
    @test 1 == 1
    @test mean(abs.(Ymap - Yml)) < 0.1
    @test mean(abs.(Ymap - Y)) < 0.1
    @test mean(abs.(Ypost - Y)) < 0.1
    @test mean(abs.(Yml - Y)) < 0.1
    @test mean(abs.(Ypredfun - Y)) < 0.1
end

@testset "Test marginal posterior" begin

    Y, X = genfakedata()
    Bpost1 = mean([rand(genposterior(Y, X)) for i in 1:1000])
    Bpost2 = mean([rand(genposterior(Y, X)) for i in 1:1000])
    Bmar1 = mean(rand(genmarginalposterior(Y, X), 1000))
    Bmar2 = mean(rand(genmarginalposterior(Y, X), 1000))

end

@testset "Univariate: posterior estimation and prediction" begin
    # Simple

    # Create data
    n = 20
    X = randn(n, 2)
    X[:, 1] .= 0:(n-1)
    X[:, 2] .= 1
    y = X[:, 1] * 0.5 + X[:, 2] * 2 + randn(n)

    # Calculate posterior parameters and predictions
    ωd = posterior(X, y, [0, 0], [0.1, 0.1], 1)
    ω = rand(ωd, 1000)
    ŷ = X * ω

    # Create predictive distribution
    predf = posteriorpred(X, y, [0, 0], [0.1, 0.1], 1)
    ȳ = rand(predf(X[3, :]), 1000)

    # Make sure they are the same
    @test abs(mean(ŷ[3, :] - ȳ)) < 0.2

end

# @testset "Energy Efficiency Dataset" begin
#
#     df = CSV.read("./data/ENB2012_data.csv", DataFrame)
#     X = Matrix(df[:, 1:8])
#     Y = Matrix(df[:, 9:10])
#     X = hcat(X, ones(size(X, 1))) # Add intercept
#
#     # Set priors
#     k = size(X, 2)
#     m = size(Y, 2)
#     B₀ = zeros(k, m)
#     Λ₀ = Matrix{Float64}(I, k, k)
#     V₀ = Matrix{Float64}(I, m, m)
#     ν₀ = 1
#     ξ = ones(m)
#
#     μd1 = mvpostμ(Y, X, Λ₀, V₀, ν₀, B₀, ξ)
#     μd2 = mvmarμ(Y, X, Λ₀, V₀, ν₀, B₀, ξ)
#     myf = generatepredictfn(Y, X, Λ₀, V₀, ν₀, B₀, ξ)
#
# end
#
#

