# Quickstart

## What is this?

This package has the ambition to make it easy for you to create Bayesian normal
multivariate multiple linear regression models using conjugate priors. The
package is based upon a multivariate gaussian likelihood and wishhart priors for
the parameters. This allows us to express the posterior distribution in closed
form.

```math
p(\theta|Y,X)=\mathbf{MN}(\theta_n, \Lambda_{n}^{-1}, \Sigma)
```

This supports the scientific process of modeling where you state your
assumptions before you observe the data. 

```mermaid
graph TD
  A[Problem] -->|Formulate hypothesis| C{Hypothesis}
  C -->|Assumptions| D[Prior]
  C -->|Data distribution| E[Likelihood]
  E --> F[Posterior]
  D --> F
```

## Bayesian Univariate Multiple Linear Regression

Let's start out easily with the univariate case and have a look at the good old
single variable linear regression (which easily extends to multiple variable).
The following example below shows you how to create some dummy data and get the
posterior distribution of the parameters of the model which in this case is
denoted ωd.

```julia
using BayesianLinearRegression
using Plots

# Generate data
n = 20
X = randn(n, 2)
X[:, 1] .= 0:(n - 1)
X[:, 2] .= 1
y = X[:, 1] * 0.5 + X[:, 2] * 2 + randn(n)

# Get posterior distribution of parameters
ωd = posterior(X, y, [0, 0], [0.1, 0.1], 1)

p = scatter(1:length(y), y, color = :red, leg = false)
for i = 1:100
    ω = rand(ωd)
    ŷ = X * ω
    plot!(p, 1:length(y), ŷ, color = :blue, leg = false)
end
savefig("f-plot1.svg"); nothing #hide
```

The plot of each regression line corresponding to a sample from the posterior
distribution of the parameters is shown below. You can clearly see that the
model has adapted to the data given.

Now a lot of things happend in this code at first glance but it's all really
simple and easy to reason about. The first part is basically only creating the
data we're going to use. The second part is where we should pay some attention.
Here we are getting the posterior distribution of the parameters of the model by
giving it the following, in order.

- Covariates matrix
- Response variable
- Prior mean for the parameters of the model
- Prior precision for the parameters of the model
- Precision of the data generation process for the model (This will not be
  adapted)

We later sample from this distribution and create a regression line for each
sample which we plot.

Now, what would happen if we change the hyperparameter controlling the precision
of the data generating process? Well, have a look.

```julia
# Get posterior distribution of parameters
ωd = posterior(X, y, [0, 0], [0.1, 0.1], 0.1)

p = scatter(1:length(y), y, color = :red, leg = false)
for i = 1:100
    ω = rand(ωd)
    ŷ = X * ω
    plot!(p, 1:length(y), ŷ, color = :blue, leg = false)
end
savefig("f-plot2.svg"); nothing #hide
```

We can also have a look at the posterior densities for our parameters to get a
marginal perspective on each parameter. In the plot below `y1` corresponds to
the slope parameter and `y2` the intercept.

```julia
using StatsPlots

plot(density(rand(ωd, 1000)'), ylab="Density", xlab="Value")
savefig("f-plot3.svg"); nothing #hide
```

## Bayesian Multivariate Multiple Linear Regression


### Setting priors

The conjugate distribution for a Gaussian likelihood is the inverse Wishart
distribution.

```math
p(\Sigma) = \mathbf{W}^{-1}()
```

Say that you want to set a rather confident prior regarding the independence of
your parameters

```julia
rand(InverseWishart(100000, Matrix{Float32}(I, 4, 4)*100000))
```

### Predictive distribution

When using an informative Inverse wishart distribution as a prior and a Gaussian
likelihood we can get an analytical expression for the predictive distribution
of a new data point. This distribution is the Multivariate T distributions.
