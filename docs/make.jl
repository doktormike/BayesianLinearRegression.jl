using BayesianLinearRegression
using Documenter

DocMeta.setdocmeta!(BayesianLinearRegression, :DocTestSetup, :(using BayesianLinearRegression); recursive=true)

makedocs(;
    modules=[BayesianLinearRegression],
    authors="Michael Green",
    sitename="BayesianLinearRegression.jl",
    format=Documenter.HTML(;
        canonical="https://doktormike.gitlab.io/BayesianLinearRegression.jl",
        edit_link="master",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
    repo=Documenter.Remotes.GitLab("doktormike", "BayesianLinearRegression.jl")
)
