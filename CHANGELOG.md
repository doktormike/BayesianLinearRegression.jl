# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.2.2](https://gitlab.com/doktormike/BayesianLinearRegression.jl/compare/v0.2.1...v0.2.2) (2024-09-04)


### Features

* added an example for MMM. ([2041b35](https://gitlab.com/doktormike/BayesianLinearRegression.jl/commit/2041b35351908cd9a52d7a5f52d42b82702fdf43))
* added more examples and a real project. ([1f8123c](https://gitlab.com/doktormike/BayesianLinearRegression.jl/commit/1f8123cce4fb791df4573416122f9efa1f583f37))
* added multivariate energy sample data. ([49e9c4f](https://gitlab.com/doktormike/BayesianLinearRegression.jl/commit/49e9c4fb9943cb4c35ec8a244951906ccd3b52b2))


### Bug Fixes

* broken tests. ([565f5cf](https://gitlab.com/doktormike/BayesianLinearRegression.jl/commit/565f5cf26b9a51dcdd2c7e0affe818e4800ae265))
