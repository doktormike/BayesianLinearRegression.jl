using BayesianLinearRegression
using Distributions
using LinearAlgebra
using UnicodePlots

function genfakemmmdata()

        N = 52 * 3
        # Season and Macro Variable
        xs = sin.((1:N) ./ (2π))
        xe = (sin.((1:N) ./ (5 * 2π)) .+ 1.1) ./ 10
        # Media
        xm1 = rand(Binomial(20, 0.02), N) * 10000
        xm2 = rand(Binomial(20, 0.02), N) * 10000
        xm3 = rand(Binomial(20, 0.02), N) * 10000

        # Targets
        y1 = 1000 .+ 500 * xs .+ 10_000 * xe .+ 0.1 * xm1 .+ 0.1 * xm2 .+ 0.1 * xm3 .+ 300 * randn(N)
        y2 = 4000 .+ 1000 * xs .+ -10_000 * xe .+ 0.2 * xm1 .+ 0.2 * xm2 .+ 0.2 * xm3 .+ 300 * randn(N)
        # p = lineplot(y1, width=120, color=:green)
        # lineplot!(p, y2, color=:red)
        # Matricies
        y = [y1 y2]
        X = [ones(N) xs xe xm1 xm2 xm3]
        (X=X, y=y)
end

function testmmmmodel()

        # Data
        X, Y = genfakemmmdata()

        # Posterior β
        k, m = size(X)[2], size(Y)[2]
        # Assome prior β are 0
        B₀ = zeros(k, m)
        # Scale Priors for betas
        # NOTE: 1:3 is the bias, season and macro which all are in the scale of 1_000 and 10_000
        Λ₀ = Matrix{Float64}(I, k, k)
        Λ₀[:, 1:2] *= 1 / 1000 # Bias and season prior
        Λ₀[:, 3] *= 1 / 10_000 # Economy prior
        # Scale priors for between target variation
        V₀ = Matrix{Float64}(I, m, m)
        ν₀ = 1
        # Additional scaling variable (most likely not very useful) = 1
        ξ = ones(size(Y)[2])
        # Posterior as a MatrixDistibution (Each random sample pulls a matrix of size k x m)
        β = BayesianLinearRegression.mvpostμ(Y, X, Λ₀, V₀, ν₀, B₀, ξ)

        # Compare relative difference of mean to OLS (Should be very similar due to our wide priors)
        βml = estimatemlμ(Y, X)
        maximum(1 .- mean(β) ./ βml) < 0.001

        # Make prediction function
        f = generatepredictfn(Y, X, Λ₀, V₀, ν₀, B₀, ξ)
        f(X)

        # Scatterplot
        ŷ = f(X)
        scatterplot(Y[:, 1], ŷ[:, 1])
        scatterplot(Y[:, 2], ŷ[:, 2])

        # Plot temporal lines
        p = lineplot(Y[:, 1], height=30, width=120)
        lineplot!(p, ŷ[:, 1], color=:red)

        βs = rand(β, 10_000)
        gs(βs, pind, tind) = [i[pind, tind] for i in βs]

        # pairplot for intercept distribution across the two targets
        densityplot(gs(βs, 1, 1), gs(βs, 1, 2))

        if false
                # pairplots
                using PairPlots
                using GLMakie

                fig = Figure()
                table1 = (β01=gs(βs, 3, 1), β11=gs(βs, 4, 1), β21=gs(βs, 5, 1), β31=gs(βs, 6, 1),)
                table2 = (β01=gs(βs, 3, 2), β11=gs(βs, 4, 2), β21=gs(βs, 5, 2), β31=gs(βs, 6, 2),)
                pairplot(fig[1, 1], table1, table2)

                fig = Figure()
                table1 = (TV=gs(βs, 4, 1), Radio=gs(βs, 5, 1), Display=gs(βs, 6, 1),)
                pairplot(fig[1, 1], table1)
                save("densities.png", fig)
        end

end

