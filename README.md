# BayesianLinearRegression

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://doktormike.gitlab.io/BayesianLinearRegression.jl/dev)
[![pipeline status](https://gitlab.com/doktormike/BayesianLinearRegression.jl/badges/master/pipeline.svg)](https://gitlab.com/doktormike/BayesianLinearRegression.jl/-/commits/master)
[![Coverage](https://gitlab.com/doktormike/BayesianLinearRegression.jl/badges/master/coverage.svg)](https://gitlab.com/doktormike/BayesianLinearRegression.jl/commits/master)

## Overview

**BayesianLinearRegression.jl** is a Julia package for performing Multivariate Bayesian Linear Regression using conjugate priors. This package is designed for those who require a probabilistic approach to linear regression, providing full uncertainty quantification through Bayesian inference.

### Why Bayesian Linear Regression?

In contrast to ordinary least squares (OLS) regression, Bayesian Linear Regression incorporates prior beliefs about the parameters into the model, offering a robust framework to capture and propagate uncertainties. This is especially valuable in real-world scenarios where data may be sparse, noisy, or the model needs to generalize well to new data points.

```math
p(\theta|Y,X)=\mathbf{MN}(\theta_n, \Lambda_{n}^{-1}, \Sigma)
```

This supports the scientific process of modeling where you state your
assumptions before you observe the data. 

```mermaid
graph TD
  A[Problem] -->|Formulate hypothesis| C{Hypothesis}
  C -->|Assumptions| D[Prior]
  C -->|Data distribution| E[Likelihood]
  E --> F[Posterior]
  D --> F
```

## Key Features

- **Multivariate Regression:** Supports multivariate linear regression models.
- **Conjugate Priors:** Uses conjugate priors to simplify the posterior computation, providing closed-form solutions for the posterior distribution.
- **Full Uncertainty Quantification:** Returns the posterior distribution of the parameters, offering insights into the uncertainty of the estimates.
- **Easy Integration:** Designed to integrate seamlessly with other Julia packages, making it ideal for complex modeling workflows.

## Installation

To install the package, simply add it using Julia's package manager:

```julia
using Pkg
Pkg.add(url="https://gitlab.com/doktormike/BayesianLinearRegression.jl")
```

## Getting Started

In `examples/mmm.jl` there is a simple example of how to use the `BayesianLinearRegression.jl` package to perform Bayesian linear regression.

![Example of densities produced for the MMM example](examples/densities.png)

## Contributing

Contributions are welcome! Please open an issue or submit a pull request if you have any improvements or suggestions.

## License

This project is licensed under the Apache License 2 - see the [LICENSE](LICENSE) file for details.

## Contact

For any questions or inquiries, please reach out via the issues page.

